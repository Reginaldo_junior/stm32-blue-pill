//-------------------- Tipos de dados --------------------  
#define __IO volatile  // abilita RW
#define uint32_t unsigned int
#define int32_t int
#define uint16_t unsigned short
#define u16 uint16_t
#define u32 uint32_t

//--------------------  GPIOx Def --------------------
#define GPIOA ((GPIO_TypeDef * ) 0x40010800) /* GPIOx Addr Base */
#define GPIOB ((GPIO_TypeDef * ) 0x40010c00) 
#define GPIOC ((GPIO_TypeDef * ) 0x40011000) 
#define GPIOD ((GPIO_TypeDef * ) 0x40011400) 
#define GPIOE ((GPIO_TypeDef * ) 0x40011800) 
#define GPIOF ((GPIO_TypeDef * ) 0x40011c00) 
#define GPIOG ((GPIO_TypeDef * ) 0x40012000) 
/* Endereço localizado no arq sfr_100xx.asm linha 646
 *
 * para ativar um pino tem que:
 * GPIOB->ODR |= 1UL<<6 // seta "1" o bit 6 que é de 0 à 15
 */

//-------------------- TIMx Def -------------------- 
#define TIM1 ((TIM_TypeDef * ) 0x40012c00) /* TIMx Addr Base */
#define TIM2 ((TIM_TypeDef * ) 0x40000000) 
#define TIM3 ((TIM_TypeDef * ) 0x40000400) 
#define TIM4 ((TIM_TypeDef * ) 0x40000800) 
#define TIM5 ((TIM_TypeDef * ) 0x40000c00) 
#define TIM6 ((TIM_TypeDef * ) 0x40001000) 
#define TIM7 ((TIM_TypeDef * ) 0x40001400) 
#define TIM8 ((TIM_TypeDef * ) 0x40013400) 
#define TIM9 ((TIM_TypeDef * ) 0x40014C00) 
#define TIM10 ((TIM_TypeDef * ) 0x40015000) 
#define TIM11 ((TIM_TypeDef * ) 0x40015400) 
#define TIM12 ((TIM_TypeDef * ) 0x40001800) 
#define TIM13 ((TIM_TypeDef * ) 0x40001c00) 
#define TIM14 ((TIM_TypeDef * ) 0x40002000) 

//-------------------- RCC Def -------------------- 
#define RCC (( RCC_TypeDef *) 0x40021000) /* RCC Addr Base */


#define STACKINT	0x20000000
//-------------------- Area de funções ---------------  
int32_t main (void);

typedef struct{
	__IO u32 CRL ; 		// 0x00 MODO de operação Port configuration register low
	__IO u32 CRH ; 		// 0x04 Port configuration register high
	__IO u32 IDR ;		// 0x08 Port input data register
	__IO u32 ODR ;		// 0x0c Port output data register
	__IO u32 BSRR ;		// 0x10 Port bit set/reset register
	__IO u32 BRR ;		// 0x14 Port bit reset register
	__IO u32 LCKR ;		// 0x18 Port configuration lock register

} GPIO_TypeDef ;

typedef struct{

	__IO u32 CR1; 		// 0x00
	__IO u32 CR2;		// 0x04
	__IO u32 SMCR;		// 0x08
	__IO u32 DIER;		// 0x0c
	__IO u32 SR; 		// 0x10
	__IO u32 EGR; 		// 0x14
	__IO u32 CCMR1;		// 0x18 ->  input/output capture mode
	__IO u32 CCMR2;		// 0x1c ->  input/output capture mode
	__IO u32 CCER; 		// 0x20 
	__IO u32 CNT; 		// 0x24
	__IO u32 PSC;		// 0x28
	__IO u32 ARR;		// 0x2c
	__IO u32 RCR;		// 0x30
	__IO u32 CCR1; 		// 0x34
	__IO u32 CCR2;		// 0x38 
	__IO u32 CCR3;		// 0x3c
	__IO u32 CCR4;		// 0x40
	__IO u32 BDTR;		// 0x44
	__IO u32 DCR;		// 0x48
	__IO u32 DMAR;		// 0x4c

} TIM_TypeDef ;

typedef struct{
//		       		  Offset
	__IO u32 CR; 		// 0x00
	__IO u32 CFGR;		// 0x04 
	__IO u32 CIR;		// 0x08
	__IO u32 APB2RSTR;	// 0x0c
	__IO u32 APB1RSTR;	// 0x10
	__IO u32 AHBENR;	// 0x14
	__IO u32 APB2ENR;	// 0x18
	__IO u32 APB1ENR;	// 0x1c
	__IO u32 BDCR;		// 0x20
	__IO u32 CSR;		// 0x24

} RCC_TypeDef;

void Delay ( __IO u32 valor ) { // volatile uint32_t valor
	for ( valor ; valor > 0 ; valor -- ){
		//reseta o timer
		TIM3->EGR |= 0x0001; 

		/* Espera até que o timer chegue à 1000
		 * O valor 1000 é por causa que, o timer está 
		 * funcionando à 1Mhz e 1000 gera 1ms */
		while ( TIM3->CNT < 1000) ;
	}
}

// tabela de vetores pagina 197 RM0008
uint32_t (* const vectors_table[])
	__attribute__ ((section(".vectoris"))) = {
	(uint32_t *) 0x20000800,        /* 0x000 Stack Pointer                   */
	(uint32_t *) main,              /* 0x004 Reset                           */
	0,                              /* 0x008 Non maskable interrupt          */
	0,                              /* 0x00C HardFault                       */
	0,                              /* 0x010 Memory Management               */
	0,                              /* 0x014 BusFault                        */
	0,                              /* 0x018 UsageFault                      */
	0,                              /* 0x01C Reserved                        */
	0,                              /* 0x020 Reserved                        */
	0,                              /* 0x024 Reserved                        */
	0,                              /* 0x028 Reserved                        */
	0,                              /* 0x02C System service call             */
	0,                              /* 0x030 Debug Monitor                   */
	0,                              /* 0x034 Reserved                        */
	0,                              /* 0x038 PendSV                          */
	0,                              /* 0x03C System tick timer               */
	0,                              /* 0x040 Window watchdog                 */
	0,                              /* 0x044 PVD through EXTI Line detection */
	0,                              /* 0x048 Tamper                          */
	0,                              /* 0x04C RTC global                      */
	0,                              /* 0x050 FLASH global                    */
	0,                              /* 0x054 RCC global                      */
	0,                              /* 0x058 EXTI Line0                      */
	0,                              /* 0x05C EXTI Line1                      */
	0,                              /* 0x060 EXTI Line2                      */
	0,                              /* 0x064 EXTI Line3                      */
	0,                              /* 0x068 EXTI Line4                      */
	0,                              /* 0x06C DMA1_Ch1                        */
	0,                              /* 0x070 DMA1_Ch2                        */
	0,                              /* 0x074 DMA1_Ch3                        */
	0,                              /* 0x078 DMA1_Ch4                        */
	0,                              /* 0x07C DMA1_Ch5                        */
	0,                              /* 0x080 DMA1_Ch6                        */
	0,                              /* 0x084 DMA1_Ch7                        */
	0,                              /* 0x088 ADC1 and ADC2 global            */
	0,                              /* 0x08C CAN1_TX                         */
	0,                              /* 0x090 CAN1_RX0                        */
	0,                              /* 0x094 CAN1_RX1                        */
	0,                              /* 0x098 CAN1_SCE                        */
	0,                              /* 0x09C EXTI Lines 9:5                  */
	0,                              /* 0x0A0 TIM1 Break                      */
	0,                              /* 0x0A4 TIM1 Update                     */
	0,                              /* 0x0A8 TIM1 Trigger and Communication  */
	0,                              /* 0x0AC TIM1 Capture Compare            */
	0,                              /* 0x0B0 TIM2                            */
	(u32 *) 0x40000400, //(uint32_t *) tim3_handler,      /* 0x0B4 TIM3                            */
	0,                              /* 0x0B8 TIM4                            */
	0,                              /* 0x0BC I2C1 event                      */
	0,                              /* 0x0C0 I2C1 error                      */
	0,                              /* 0x0C4 I2C2 event                      */
	0,                              /* 0x0C8 I2C2 error                      */
	0,                              /* 0x0CC SPI1                            */
	0,                              /* 0x0D0 SPI2                            */
	0,                              /* 0x0D4 USART1                          */
	0,                              /* 0x0D8 USART2                          */
	0,                              /* 0x0DC USART3                          */
	0,                              /* 0x0E0 EXTI Lines 15:10                */
	0,                              /* 0x0E4 RTC alarm through EXTI line     */
	0,                              /* 49  USB OTG FS Wakeup through EXTI  */
	0,                              /* -   Reserved                        */
	0,                              /* -   Reserved                        */
	0,                              /* -   Reserved                        */
	0,                              /* -   Reserved                        */
	0,                              /* -   Reserved                        */
	0,                              /* -   Reserved                        */
	0,                              /* -   Reserved                        */
	0,                              /* 57  TIM5                            */
	0,                              /* 58  SPI3                            */
	0,                              /* 59  USART4                          */
	0,                              /* 60  USART5                          */
	0,                              /* 61  TIM6                            */
	0,                              /* 62  TIM7                            */
	0,                              /* 63  DMA2_Ch1                        */
	0,                              /* 64  DMA2_Ch2                        */
	0,                              /* 65  DMA2_Ch3                        */
	0,                              /* 66  DMA2_Ch4                        */
	0,                              /* 67  DMA2_Ch5                        */
	0,                              /* 68  Ethernet                        */
	0,                              /* 69  Ethernet wakeup                 */
	0,                              /* 70  CAN2_TX                         */
	0,                              /* 71  CAN2_RX0                        */
	0,                              /* 72  CAN2_RX1                        */
	0,                              /* 73  CAN2_SCE                        */
	0,                              /* 74  USB OTG FS                      */

};


int main (void){
	
//--------------- Ativando clock ----------
	// void set_system_clock_to_25Mhz(void)	
	RCC->CR |= (1 << 16); // abilita HSE
	while (!( RCC->CR & (1 << 17 ) ) ); /*Espera até a linha de 
					      cima estar resetada 
					      automaricamente e o 
					      RCC->CR[17] ser setado*/
	RCC->CFGR |= (1 << 0); /* Informa a fonte de clock,
				  no caso interno*/

//--------------- RCC Ativando o clocl da GPIOx ----------
	RCC->APB2ENR |= ( 1<<2 ) ; // Abilita a GPIOA
//	RCC->APB2ENR |= 1UL<<3 ; // Abilita a GPIOB 
	RCC->APB2ENR |= ( 1<<4 ) ; // Abilita a GPIOC
	// (1<<2) | ( 1<<4 ) ;
	//
/*  | | | | | | | | | | | | | | | | | | | | | | | | | | |1|1|R|0| -> Pag 91 Datasheet F100xx */


//--------------- Configurando saídas GPIOx ----------
//	GPIO_Init (GPIOC);
	/* 2 bits por pinos, 00 Input digital; 01 Out Digital
      10 Alt function mode 11 analogic mode */

	GPIOC->CRH &= 0x00000000 ; // Limpeza
       	
	/* Defini a soma bit-a-bit para ativar os pinos
	  14 , 13 modo output - input terá mais parâmetros */
	GPIOC->CRH |= ( (1 << 25 ) | ( 1 << 21 )  );
	GPIOC->ODR &= ~( 1 << 14 ); // Reset no pino 14
	GPIOC->ODR |= ( 1 << 13) ; //set no pino 13
	
//-------------------- Configurando o TIM -> RCC ---------- 
	// Abilita o clock RCC para TIM3
	RCC->APB1ENR |= ( 1 << 1 ); // TIM3
	TIM3->CR1 = 0x0000;

	/* fCK_PSC / (PSC[15:0] +1)) 
	 * 72 Mhz / 71 + 1 = 1Mhz time clock speed 
	 * logo: 25 Mhz / 24 + 1 = 1Mhz */
	TIM3->PSC = 24 ; // clock Mhz -1

	/*Definindo como valor Máx 0xffff desde que 
	   verificamos manualmente
	 * Se o valor atingir 1000 na função Delay */
	TIM3->ARR = 0xffff; 

	// por fim, abilitando-o
	TIM3->CR1 |= ( 1<<0 );

//-------------------- Loop infinito ----------  
	while (1){
		Delay( 1000 );
		GPIOC->ODR ^= ( 1<<14 ); /* Troca o estado
					    do pino 14 */
		GPIOC->ODR ^= ( 1<<13 ); 

	}

	//o // erro de proposito
}
