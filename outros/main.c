#include<stdio.h>

/* O "LED" está recebendo a posição de memória "0xD..." 
 * "Volatile" indica que a variável pode ser modificada por fora do programa/sem ele saber 
 * */

#define LED *( (volatile unsigned int *) ( 0xDFFF000C ) ) 

int main (  void  ){
	unsigned int i; // contador do loop para a função delay.
	volatile int j; // usado para o compilador não otimizar o codigo *isso.

	while (1){
		LED = 0x00; // A posição de mem apontada por "LED" recebe o valor 0 ou 0x00
		for ( i = 0 ; i < 10 ; i++ ) { j = 0; } //DELAY 
		LED = 0x01; 
		for ( i = 0 ; i < 10 ; i++ ) { j = 0; } //DELAY   
	}
	return 0;
}
