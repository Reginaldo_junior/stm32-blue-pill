//-------------------- Tipos de dados --------------------  
#define __IO volatile  // abilita RW
#define uint32_t unsigned int
#define u32 unsigned int
#define int32_t int

#define uint16_t unsigned short
#define u16 unsigned short

//--------------------  GPIOx Def --------------------
#define GPIOA ((GPIO_TypeDef * ) 0x40010800) /* GPIOx Addr Base */
#define GPIOB ((GPIO_TypeDef * ) 0x40010c00) 
#define GPIOC ((GPIO_TypeDef * ) 0x40011000) 
#define GPIOD ((GPIO_TypeDef * ) 0x40011400) 
#define GPIOE ((GPIO_TypeDef * ) 0x40011800) 
#define GPIOF ((GPIO_TypeDef * ) 0x40011c00) 
#define GPIOG ((GPIO_TypeDef * ) 0x40012000) 
/* Endereço localizado no arq sfr_100xx.asm linha 646
 *
 * para ativar um pino tem que:
 * GPIOB->ODR |= 1UL<<6 // seta "1" o bit 6 que é de 0 à 15
 */

//-------------------- TIMx Def -------------------- 
#define TIM1 ((TIM_TypeDef * ) 0x40012c00) /* TIMx Addr Base */
#define TIM2 ((TIM_TypeDef * ) 0x40000000) 
#define TIM3 ((TIM_TypeDef * ) 0x40000400) 
#define TIM4 ((TIM_TypeDef * ) 0x40000800) 
#define TIM5 ((TIM_TypeDef * ) 0x40000c00) 
#define TIM6 ((TIM_TypeDef * ) 0x40001000) 
#define TIM7 ((TIM_TypeDef * ) 0x40001400) 
#define TIM8 ((TIM_TypeDef * ) 0x40013400) 
#define TIM9 ((TIM_TypeDef * ) 0x40014C00) 
#define TIM10 ((TIM_TypeDef * ) 0x40015000) 
#define TIM11 ((TIM_TypeDef * ) 0x40015400) 
#define TIM12 ((TIM_TypeDef * ) 0x40001800) 
#define TIM13 ((TIM_TypeDef * ) 0x40001c00) 
#define TIM14 ((TIM_TypeDef * ) 0x40002000) 

//-------------------- RCC Def -------------------- 
#define RCC (( RCC_TypeDef *) 0x40021000) /* RCC Addr Base */


#define STACKINT	0x20000000
//-------------------- Area de funções ---------------  
int32_t main (void);

typedef struct{
	__IO u32 CRL ; 		// 0x00 MODO de operação Port configuration register low
	__IO u32 CRH ; 		// 0x04 Port configuration register high
	__IO u32 IDR ;		// 0x08 Port input data register
	__IO u32 ODR ;		// 0x0c Port output data register
	__IO u32 BSRR ;		// 0x10 Port bit set/reset register
	__IO u32 BRR ;		// 0x14 Port bit reset register
	__IO u32 LCKR ;		// 0x18 Port configuration lock register

} GPIO_TypeDef ;

typedef struct{

	__IO u32 CR1; 		// 0x00
	__IO u32 CR2;		// 0x04
	__IO u32 SMCR;		// 0x08
	__IO u32 DIER;		// 0x0c
	__IO u32 SR; 		// 0x10
	__IO u32 EGR; 		// 0x14
	__IO u32 CCMR1;		// 0x18 ->  input/output capture mode
	__IO u32 CCMR2;		// 0x1c ->  input/output capture mode
	__IO u32 CCER; 		// 0x20 
	__IO u32 CNT; 		// 0x24
	__IO u32 PSC;		// 0x28
	__IO u32 ARR;		// 0x2c
	__IO u32 RCR;		// 0x30
	__IO u32 CCR1; 		// 0x34
	__IO u32 CCR2;		// 0x38 
	__IO u32 CCR3;		// 0x3c
	__IO u32 CCR4;		// 0x40
	__IO u32 BDTR;		// 0x44
	__IO u32 DCR;		// 0x48
	__IO u32 DMAR;		// 0x4c

} TIM_TypeDef ;

typedef struct{
//		       		  Offset
	__IO u32 CR; 		// 0x00
	__IO u32 CFGR;		// 0x04 
	__IO u32 CIR;		// 0x08
	__IO u32 APB2RSTR;	// 0x0c
	__IO u32 APB1RSTR;	// 0x10
	__IO u32 AHBENR;	// 0x14
	__IO u32 APB2ENR;	// 0x18
	__IO u32 APB1ENR;	// 0x1c
	__IO u32 BDCR;		// 0x20
	__IO u32 CSR;		// 0x24

} RCC_TypeDef;

void SystemInit (void) {

  /* Reset the RCC clock configuration to the default reset state(for debug purpose) */
  /* Set HSION bit */
  RCC->CR |= (uint32_t)0x00000001;

  /* Reset SW, HPRE, PPRE1, PPRE2, ADCPRE and MCO bits */
  RCC->CFGR &= (uint32_t)0xF8FF0000;
 
  /* Reset HSEON, CSSON and PLLON bits */
  RCC->CR &= (uint32_t)0xFEF6FFFF;

  /* Reset HSEBYP bit */
  RCC->CR &= (uint32_t)0xFFFBFFFF;

  /* Reset PLLSRC, PLLXTPRE, PLLMUL and USBPRE/OTGFSPRE bits */
  RCC->CFGR &= (uint32_t)0xFF80FFFF;

  /* Disable all interrupts and clear pending bits  */
  RCC->CIR = 0x009F0000;
 }


void Delay ( __IO u32 valor ) { // volatile uint32_t valor
	for ( valor ; valor > 0 ; valor -- ){
		//reseta o timer
		TIM3->EGR |= 0x0001; 

		/* Espera até que o timer chegue à 1000
		 * O valor 1000 é por causa que, o timer está 
		 * funcionando à 1Mhz e 1000 gera 1ms */

		while ( TIM3->CNT < valor ) ;
	}
}

// tabela de vetores pagina 197 RM0008

void init_RCC (void) {

//--------------- Ativando clock ----------
	// void set_system_clock_to_25Mhz(void)	
	RCC->CR |= (1 << 16); // abilita HSE
	while (!( RCC->CR & (1 << 17 ) ) ); /*Espera até a linha de 
					      cima estar resetada 
					      automaricamente e o 
					      RCC->CR[17] ser setado*/
	RCC->CFGR |= (1 << 0); /* Informa a fonte de clock,
				  no caso interno*/

}

void init_GPIO (){

//--------------- RCC Ativando o clocl da GPIOx ----------
	RCC->APB2ENR |= ( 1<<2 ) ; // Abilita a GPIOA
//	RCC->APB2ENR |= 1UL<<3 ; // Abilita a GPIOB 
	RCC->APB2ENR |= ( 1<<4 ) ; // Abilita a GPIOC
	// (1<<2) | ( 1<<4 ) ;
	//
/*  | | | | | | | | | | | | | | | | | | | | | | | | | | |1|1|R|0| -> Pag 91 Datasheet F100xx */

}

void out_GPIO (void) {

//--------------- Configurando saídas GPIOx ----------
//	GPIO_Init (GPIOC);
	/* 2 bits por pinos, 00 Input digital; 01 Out Digital
      10 Alt function mode 11 analogic mode */

	GPIOC->CRH &= 0x00000000 ; // Limpeza
       	
	/* Defini a soma bit-a-bit para ativar os pinos
	  14 , 13 modo output - input terá mais parâmetros */
	GPIOC->CRH |= ( (1 << 25 ) | ( 1 << 21 )  );
	GPIOC->ODR &= ~( 1 << 14 ); // Reset no pino 14
	GPIOC->ODR |= ( 1 << 13) ; //set no pino 13
}

void init_TIM (void) {
//-------------------- Configurando o TIM -> RCC ---------- 
	// Abilita o clock RCC para TIM3
	RCC->APB1ENR |= ( 1 << 1 ); // TIM3
	TIM3->CR1 = 0x0000;

	/* fCK_PSC / (PSC[15:0] +1)) 
	 * 72 Mhz / 71 + 1 = 1Mhz time clock speed 
	 * logo: 25 Mhz / 24 + 1 = 1Mhz */
	TIM3->PSC = 24 ; // clock Mhz -1

	/*Definindo como valor Máx 0xffff desde que 
	   verificamos manualmente
	 * Se o valor atingir 1000 na função Delay */
	TIM3->ARR = 0xffff; 

	// por fim, abilitando-o
	TIM3->CR1 |= ( 1<<0 );
}


int main (void){
	init_RCC ();
	init_GPIO ();
	out_GPIO ();
	init_TIM ();	

//-------------------- Loop infinito ----------  
	while (1){
		Delay( 1000 );
		GPIOC->ODR ^= ( 1<<14 ); /* Troca o estado
					    do pino 14 */
		GPIOC->ODR ^= ( 1<<13 ); 

	}
	// o // erro de proposito
}
